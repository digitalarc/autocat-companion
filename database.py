import io
import os
import shutil

from pathlib import Path

import data
import luabins

from peewee import *

config = data.ConfigData()
db = SqliteDatabase(config.get('save_path'))


class LuabinField(BlobField):

    def db_value(self, value):
        return value if value is None else luabins.encode_luabins(value)

    def python_value(self, value):
        if value is not None:
            if isinstance(value, str) and not value.startswith('b\'\\'):
                value = value.encode('raw_unicode_escape')

            return luabins.decode_luabins(io.BytesIO(value))


class BaseModel(Model):
    class Meta:
        database = db


class Families(BaseModel):
    block_config = TextField(column_name='BlockConfig', null=True)
    block_material = TextField(column_name='BlockMaterial', null=True)
    block_type = TextField(column_name='BlockType', null=True)
    bore = FloatField(column_name='Bore', null=True)
    game_version = IntegerField(column_name='GameVersion', null=True)
    head = TextField(column_name='Head', null=True)
    head_material = TextField(column_name='HeadMaterial', null=True)
    internal_days = IntegerField(column_name='InternalDays', null=True)
    name = TextField(column_name='Name', null=True)
    quality_family = IntegerField(column_name='QualityFamily', null=True)
    stroke = FloatField(column_name='Stroke', null=True)
    tech_pool = LuabinField(column_name='TechPool', null=True)
    uid = TextField(column_name='UID', primary_key=True)
    vvl = TextField(column_name='VVL', null=True)
    valves = TextField(column_name='Valves', null=True)

    class Meta:
        table_name = 'Families'


class Variants(BaseModel):
    afr = FloatField(column_name='AFR', null=True)
    afr_lean = FloatField(column_name='AFRLean', null=True)
    ar_ratio = FloatField(column_name='ARRatio', null=True)
    aspiration = TextField(column_name='Aspiration', null=True)
    aspiration_option = TextField(column_name='AspirationOption', null=True)
    aspiration_type = TextField(column_name='AspirationType', null=True)
    boost_cut_off = FloatField(column_name='BoostCutOff', null=True)
    bore = FloatField(column_name='Bore', null=True)
    cam_profile_setting = FloatField(column_name='CamProfileSetting', null=True)
    capacity = FloatField(column_name='Capacity', null=True)
    cat = TextField(column_name='Cat', null=True)
    compression = FloatField(column_name='Compression', null=True)
    compressor_fraction = FloatField(column_name='CompressorFraction', null=True)
    conrods = TextField(column_name='Conrods', null=True)
    crank = TextField(column_name='Crank', null=True)
    exhaust_bypass_valves = TextField(column_name='ExhaustBypassValves', null=True)
    exhaust_count = TextField(column_name='ExhaustCount', null=True)
    exhaust_diameter = FloatField(column_name='ExhaustDiameter', null=True)
    fuid = ForeignKeyField(column_name='FUID', field='uid', model=Families)
    fuel_system = TextField(column_name='FuelSystem', null=True)
    fuel_system_type = TextField(column_name='FuelSystemType', null=True)
    fuel_type = TextField(column_name='FuelType', null=True)
    game_version = IntegerField(column_name='GameVersion', null=True)
    headers = TextField(column_name='Headers', null=True)
    ignition_timing_setting = FloatField(column_name='IgnitionTimingSetting', null=True)
    intake = TextField(column_name='Intake', null=True)
    intake_manifold = TextField(column_name='IntakeManifold', null=True)
    intercooler_setting = FloatField(column_name='IntercoolerSetting', constraints=[SQL("DEFAULT 0.5")], null=True)
    internal_days = IntegerField(column_name='InternalDays', null=True)
    muffler1 = TextField(column_name='Muffler1', null=True)
    muffler2 = TextField(column_name='Muffler2', null=True)
    name = TextField(column_name='Name', null=True)
    pistons = TextField(column_name='Pistons', null=True)
    quality_aspiration = IntegerField(column_name='QualityAspiration', null=True)
    quality_bottom_end = IntegerField(column_name='QualityBottomEnd', null=True)
    quality_exhaust = IntegerField(column_name='QualityExhaust', null=True)
    quality_fuel_system = IntegerField(column_name='QualityFuelSystem', null=True)
    quality_top_end = IntegerField(column_name='QualityTopEnd', null=True)
    rpm_limit = FloatField(column_name='RPMLimit', null=True)
    rocker_settings = LuabinField(column_name='RockerSettings', null=True)
    stroke = FloatField(column_name='Stroke', null=True)
    tags = TextField(column_name='Tags', null=True)
    tech_pool = LuabinField(column_name='TechPool', null=True)
    turbine_fraction = FloatField(column_name='TurbineFraction', null=True)
    uid = TextField(column_name='UID', primary_key=True)
    vvl_cam_profile_setting = FloatField(column_name='VVLCamProfileSetting', null=True)
    vvt = TextField(column_name='VVT', null=True)

    class Meta:
        table_name = 'Variants'


class Companies(BaseModel):
    name = TextField(column_name='Name', null=True)
    uid = TextField(column_name='UID', primary_key=True)

    class Meta:
        table_name = 'Companies'


class CompanyVariants(BaseModel):
    cuid = ForeignKeyField(column_name='CUID', field='uid', model=Companies)
    owner = IntegerField(column_name='Owner', null=True)
    vuid = ForeignKeyField(column_name='VUID', field='uid', model=Variants, unique=True)

    class Meta:
        table_name = 'CompanyVariants'
        primary_key = False


class CompanyFamilies(BaseModel):
    cuid = ForeignKeyField(column_name='CUID', field='uid', model=Companies)
    fuid = ForeignKeyField(column_name='FUID', field='uid', model=Families, unique=True)
    owner = IntegerField(column_name='Owner', null=True)

    class Meta:
        table_name = 'CompanyFamilies'
        primary_key = False


class SaveBackup:

    def __init__(self):
        self.save_file = config.get('save_path')

        homedir = str(Path.home())
        self.backup_folder = os.path.join(homedir, 'AppData\\Local\\DigitalArc Studio\\Autocat Companion\\Backups')

        if not os.path.isdir(self.backup_folder):
            self.__creat_backup_folder()

    def get_backups(self):
        """
        Attempt to make a backup of the save_file
        :return: list
        """
        files = [f for f in os.listdir(self.backup_folder) if os.path.isfile(os.path.join(self.backup_folder, f))]
        return files

    def make_backup(self):
        """
        Attempt to make a backup of the save_file
        :return: None
        """

        if len(self.get_backups()) > 3:
            oldest_file = min([os.path.join(self.backup_folder, x) for x in self.get_backups()], key=os.path.getctime)
            os.remove(oldest_file)

        counter = 0
        filename = "save{}.bak"
        while os.path.isfile(os.path.join(self.backup_folder, filename.format(counter))):
            counter += 1
        filename = filename.format(counter)

        shutil.copyfile(self.save_file, os.path.join(self.backup_folder, filename))

    def restore(self):
        """
        Attempt to restore a backup
        :return: boolean
        """

        newest_file = max([os.path.join(self.backup_folder, x) for x in self.get_backups()], key=os.path.getctime)
        os.remove(self.save_file)
        shutil.copyfile(newest_file, self.save_file)

    def __creat_backup_folder(self):
        """
        :return: None
        """
        os.makedirs(self.backup_folder, exist_ok=True)
