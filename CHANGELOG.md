## 1.1.0

* Implements auto restore of backup on failed import. Can be turned off in options.
* Check if game is running and stop of it is.

Released 2021-03-01

## 1.0.2

* Fixed problem with imported engines made the exporting corrupt. Sadly had to remove the ability to import engine look. (They were wrong anyways) See https://gitlab.com/digitalarc/autocat-companion/-/issues/1

Released 2021-02-28

## 1.0.1

* Fixing some pipeline stuff

Released 2021-02-27

## 1.0.0

* First release!

Released 2021-02-27