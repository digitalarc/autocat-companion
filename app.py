import ctypes
import data
import json
import logging
import os
import re
import subprocess
import sys
import tkinter as tk
import unicodedata

from pathlib import Path
from tkinter import filedialog

from database import db, SaveBackup, Variants, Families, CompanyFamilies, CompanyVariants, Companies, DoesNotExist
from prompt_toolkit.styles import Style
from questionary import prompt, print as qprint, confirm, select

root = tk.Tk()
root.overrideredirect(1)
root.withdraw()

data_dir = data.create_data_dir()

logger = logging.getLogger('autocat-companion')
logger.propagate = False
logger.setLevel(logging.INFO)
if not logger.handlers:
    fh = logging.FileHandler(filename=os.path.join(data_dir, 'history.log'))
    fh.setLevel(logging.INFO)
    formatter = logging.Formatter('[%(asctime)s][%(levelname)s] %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    fh.setFormatter(formatter)
    logger.addHandler(fh)

logger.info('Init')

version = '1.1.0'

style = Style([
    ('qmark', 'fg:#462222 bold'),  # token in front of the question
    ('question', 'bold'),  # question text
    ('answer', 'fg:#462222 bold'),  # submitted answer text behind the question
    ('pointer', 'fg:#462222 bold'),  # pointer used in select and checkbox prompts
    ('highlighted', 'fg:#462222 bold'),  # pointed-at choice in select and checkbox prompts
    ('selected', 'fg:#FFFFFF bold'),  # style for a selected item of a checkbox
    ('separator', 'fg:#462222'),  # separator in lists
    ('instruction', ''),  # user instructions for select, rawselect, checkbox
    ('text', ''),  # plain text
    ('disabled', 'fg:#858585 italic')  # disabled choices for select and checkbox prompts
])


def is_automation_running():
    names = [
        'AutomationGame-Win64',
        'AutomationGame-Win32',
    ]

    call = 'tasklist', '/fo', 'list'

    # use buildin check_output right away
    output = subprocess.check_output(call).decode()

    # Get all processes
    processes_list = [x.split('Image Name:   ')[1] for x
                      in output.strip().split('\r\n') if x.startswith('Image Name:   ')]

    # Return if the process was found or not
    return len([e for e in names if e in '\n'.join(processes_list)]) > 0


def print_maincolor(msg):
    qprint(msg, style="fg:#462222")


def print_white(msg):
    qprint(msg, style="fg:#ffffff")


def print_success(msg):
    qprint(msg, style="fg:#16c60c")


def print_warning(msg):
    qprint(msg, style="fg:#ffff00")


def print_error(msg):
    qprint(msg, style="fg:darkred")
    

def clean_screen():
    os.system('cls')


def sanitize_filename(value, allow_unicode=False):
    """
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize('NFKC', value)
    else:
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value.lower())
    return re.sub(r'[-\s]+', '-', value).strip('-_')


def open_save(initialdir="/"):
    filename = filedialog.askopenfilename(initialdir=initialdir,
                                          title="Select a Save file",
                                          filetypes=[
                                              ("Automation save", "*.db")
                                          ])
    return filename


def open_export_file(initialdir=os.path.join(str(Path.home()), 'Documents')):
    filename = filedialog.askopenfilename(initialdir=initialdir,
                                          title="Select a engine to import",
                                          filetypes=[
                                              ("Autocat exports", "*.json")
                                          ])
    return filename


def open_export_path(initialdir=os.path.join(str(Path.home()), 'Documents')):
    dir = filedialog.askdirectory(initialdir=initialdir, title="Select folder to export to")
    return dir


def export_engine(uid, path):
    """
    :param str uid: the UID of the engine to export
    :param str path: the path where the engine is going to be exported to
    """

    conn = db.connection()
    conn.text_factory = bytes

    variant = Variants.get(Variants.uid == uid)

    engine_data = {
        'engine_name': f"{variant.fuid.name} {variant.name}",
        'engine_specs': {
            'uid': variant.uid,
            'fuid': variant.fuid.uid,
            'name': variant.name,
            'game_version': variant.game_version,
            'internal_days': variant.internal_days,
            'crank': variant.crank,
            'conrods': variant.conrods,
            'pistons': variant.pistons,
            'vvt': variant.vvt,
            'aspiration': variant.aspiration,
            'aspiration_type': variant.aspiration_type,
            'aspiration_option': variant.aspiration_option,
            'intercooler_setting': variant.intercooler_setting,
            'fuel_system_type': variant.fuel_system_type,
            'fuel_system': variant.fuel_system,
            'intake_manifold': variant.intake_manifold,
            'intake': variant.intake,
            'fuel_type': variant.fuel_type,
            'headers': variant.headers,
            'exhaust_count': variant.exhaust_count,
            'exhaust_bypass_valves': variant.exhaust_bypass_valves,
            'cat': variant.cat,
            'muffler1': variant.muffler1,
            'muffler2': variant.muffler2,
            'bore': variant.bore,
            'stroke': variant.stroke,
            'capacity': variant.capacity,
            'compression': variant.compression,
            'cam_profile_setting': variant.cam_profile_setting,
            'vvl_cam_profile_setting': variant.vvl_cam_profile_setting,
            'afr': variant.afr,
            'afr_lean': variant.afr_lean,
            'rpm_limit': variant.rpm_limit,
            'ignition_timing_setting': variant.ignition_timing_setting,
            'ar_ratio': variant.ar_ratio,
            'boost_cut_off': variant.boost_cut_off,
            'compressor_fraction': variant.compressor_fraction,
            'turbine_fraction': variant.turbine_fraction,
            'exhaust_diameter': variant.exhaust_diameter,
            'quality_bottom_end': variant.quality_bottom_end,
            'quality_top_end': variant.quality_top_end,
            'quality_aspiration': variant.quality_aspiration,
            'quality_fuel_system': variant.quality_fuel_system,
            'quality_exhaust': variant.quality_exhaust,
            'rocker_settings': variant.rocker_settings,
            'tech_pool': variant.tech_pool,
            'tags': variant.tags
        },
        'engine_family': {
            'uid': variant.fuid.uid,
            'name': variant.fuid.name,
            'game_version': variant.fuid.game_version,
            'internal_days': variant.fuid.internal_days,
            'block_config': variant.fuid.block_config,
            'block_material': variant.fuid.block_material,
            'block_type': variant.fuid.block_type,
            'head': variant.fuid.head,
            'head_material': variant.fuid.head_material,
            'valves': variant.fuid.valves,
            'vvl': variant.fuid.vvl,
            'bore': variant.fuid.bore,
            'stroke': variant.fuid.stroke,
            'quality_family': variant.fuid.quality_family,
            'tech_pool': variant.fuid.tech_pool
        }
    }

    filename = sanitize_filename(engine_data.get('engine_name')) + '_export.json'

    with open(os.path.join(path, filename), 'w+') as file:
        json.dump(engine_data, file, indent=4)

    return


def import_engine(file):
    """
    :param str file: the path where the engine is imported from
    """
    with open(file, 'r') as file:
        engine_data = json.load(file)

    logger.info('Making backup of automation save')
    SaveBackup().make_backup()

    conn = db.connection()
    conn.text_factory = bytes

    c = company_list()
    company = select('Select company to import engines into.', c, style=style, qmark='#').ask()
    clean_screen()

    try:
        f = Families.get(uid=engine_data.get('engine_family').get('uid'))
    except Families.DoesNotExist:
        f = Families.create(
            uid=engine_data.get('engine_family').get('uid'),
            name=engine_data.get('engine_family').get('name'),
            game_version=engine_data.get('engine_family').get('game_version'),
            internal_days=engine_data.get('engine_family').get('internal_days'),
            block_config=engine_data.get('engine_family').get('block_config'),
            block_material=engine_data.get('engine_family').get('block_material'),
            block_type=engine_data.get('engine_family').get('block_type'),
            head=engine_data.get('engine_family').get('head'),
            head_material=engine_data.get('engine_family').get('head_material'),
            valves=engine_data.get('engine_family').get('valves'),
            vvl=engine_data.get('engine_family').get('vvl'),
            bore=engine_data.get('engine_family').get('bore'),
            stroke=engine_data.get('engine_family').get('stroke'),
            quality_family=engine_data.get('engine_family').get('quality_family'),
            tech_pool=None
        )
    try:
        Variants.get(uid=engine_data.get('engine_specs').get('uid'))
    except Variants.DoesNotExist:
        Variants.create(
            uid=engine_data.get('engine_specs').get('uid'),
            fuid=f,
            name=engine_data.get('engine_specs').get('name'),
            game_version=engine_data.get('engine_specs').get('game_version'),
            internal_days=engine_data.get('engine_specs').get('internal_days'),
            crank=engine_data.get('engine_specs').get('crank'),
            conrods=engine_data.get('engine_specs').get('conrods'),
            pistons=engine_data.get('engine_specs').get('pistons'),
            vvt=engine_data.get('engine_specs').get('vvt'),
            aspiration=engine_data.get('engine_specs').get('aspiration'),
            aspiration_type=engine_data.get('engine_specs').get('aspiration_type'),
            aspiration_option=engine_data.get('engine_specs').get('aspiration_option'),
            intercooler_setting=engine_data.get('engine_specs').get('intercooler_setting'),
            fuel_system_type=engine_data.get('engine_specs').get('fuel_system_type'),
            fuel_system=engine_data.get('engine_specs').get('fuel_system'),
            intake_manifold=engine_data.get('engine_specs').get('intake_manifold'),
            intake=engine_data.get('engine_specs').get('intake'),
            fuel_type=engine_data.get('engine_specs').get('fuel_type'),
            headers=engine_data.get('engine_specs').get('headers'),
            exhaust_count=engine_data.get('engine_specs').get('exhaust_count'),
            exhaust_bypass_valves=engine_data.get('engine_specs').get('exhaust_bypass_valves'),
            cat=engine_data.get('engine_specs').get('cat'),
            muffler1=engine_data.get('engine_specs').get('muffler1'),
            muffler2=engine_data.get('engine_specs').get('muffler2'),
            bore=engine_data.get('engine_specs').get('bore'),
            stroke=engine_data.get('engine_specs').get('stroke'),
            capacity=engine_data.get('engine_specs').get('capacity'),
            compression=engine_data.get('engine_specs').get('compression'),
            cam_profile_setting=engine_data.get('engine_specs').get('cam_profile_setting'),
            vvl_cam_profile_setting=engine_data.get('engine_specs').get('vvl_cam_profile_setting'),
            afr=engine_data.get('engine_specs').get('afr'),
            afr_lean=engine_data.get('engine_specs').get('afr_lean'),
            rpm_limit=engine_data.get('engine_specs').get('rpm_limit'),
            ignition_timing_setting=engine_data.get('engine_specs').get('ignition_timing_setting'),
            ar_ratio=engine_data.get('engine_specs').get('ar_ratio'),
            boost_cut_off=engine_data.get('engine_specs').get('boost_cut_off'),
            compressor_fraction=engine_data.get('engine_specs').get('compressor_fraction'),
            turbine_fraction=engine_data.get('engine_specs').get('turbine_fraction'),
            exhaust_diameter=engine_data.get('engine_specs').get('exhaust_diameter'),
            quality_bottom_end=engine_data.get('engine_specs').get('quality_bottom_end'),
            quality_top_end=engine_data.get('engine_specs').get('quality_top_end'),
            quality_aspiration=engine_data.get('engine_specs').get('quality_aspiration'),
            quality_fuel_system=engine_data.get('engine_specs').get('quality_fuel_system'),
            quality_exhaust=engine_data.get('engine_specs').get('quality_exhaust'),
            rocker_settings=None,
            tech_pool=None,
            tags=engine_data.get('engine_specs').get('tags')
        )
    try:
        CompanyFamilies.get(cuid=company, fuid=engine_data.get('engine_family').get('uid'))
    except CompanyFamilies.DoesNotExist:
        CompanyFamilies.create(cuid=company, fuid=engine_data.get('engine_family').get('uid'), owner=1)

    try:
        CompanyVariants.get(cuid=company, vuid=engine_data.get('engine_specs').get('uid'))
    except CompanyVariants.DoesNotExist:
        CompanyVariants.create(cuid=company, vuid=engine_data.get('engine_specs').get('uid'), owner=1)

    db.close()
    return


def engine_list():
    conn = db.connection()
    conn.text_factory = bytes
    engines = [{'name': f"{v.fuid.name} {v.name}", 'value': v.uid} for v in Variants.select()]
    db.close()
    return engines


def company_list():
    conn = db.connection()
    conn.text_factory = bytes
    companies = [{'name': f"{c.name}", 'value': c.uid} for c in Companies.select()]
    db.close()
    return companies


def main():
    global config
    while True:
        options = {
            'type': 'list',
            'name': 'option',
            'message': 'Main menu',
            'qmark': '@',
            'choices': [
                {
                    'name': 'Import engines',
                    'value': 'import'
                },
                {
                    'name': 'Export engines',
                    'value': 'export'
                },
                {
                    'name': 'Restore latest backup',
                    'value': 'restore_backup',
                    'disabled': SaveBackup().get_backups() == 0
                },
                {
                    'name': 'Options',
                    'value': 'options'
                },
                {
                    'name': 'Quit',
                    'value': 'quit'
                }
            ]
        }

        answer = prompt(options, style=style)
        if not answer:
            sys.exit(0)
        clean_screen()
        option = answer['option']

        if option == 'import':

            # Check if game is running, abort if it is
            if is_automation_running():
                clean_screen()
                logger.info('Aborted import, game is running')
                print_warning('Game is running, please close it first.')
                return

            # Take a backup of the save
            try:
                SaveBackup().make_backup()
            except Exception as e:
                logger.error(f'Failed to backup save: {str(e)}')
                print_error(f'Failed to backup save.')
                return

            engine_file = open_export_file()

            try:
                import_engine(engine_file)
                clean_screen()
                logger.info(f'Imported engine {engine_file}')
                print_success(f'Imported engine {engine_file}.')
            except Exception as e:
                clean_screen()
                logger.error(f'Failed to import engine "{engine_file}" failed: {str(e)}')
                print_error(f'Failed to import engine "{engine_file}". Is this file valid?')
                if config.get('restore_backup_on_fail'):
                    logger.info(f'Restoring backup')
                    print_white('Restoring backup.')
                    try:
                        SaveBackup().restore()
                        logger.info(f'Restored backup')
                        print_success('Restored backup.')
                    except Exception as e:
                        logger.error(f'Failed to restore backup: {str(e)}')
                        print_error(f'Failed to restore backup, see log for more information.')
                return
            return

        elif option == 'export':

            # Check if game is running, abort if it is
            if is_automation_running():
                clean_screen()
                logger.info('Aborted export, game is running')
                print_warning('Game is running, please close it first.')
                return

            c = engine_list()

            options = {
                'type': 'checkbox',
                'name': 'option',
                'qmark': '@',
                'message': 'Select engines to export',
                'choices': c
            }
            print_white('Press ENTER to proceed.')
            answer = prompt(options, style=style)
            clean_screen()
            export_dir = open_export_path()
            for x in answer['option']:
                try:
                    logger.info(f'Exporting engine {x} to {export_dir}')
                    export_engine(x, export_dir)
                    logger.info(f'Successfully exported engine {x} to {export_dir}')
                    print_success(f'Successfully exported engine {x} to {export_dir}')
                    db.close()
                except DoesNotExist:
                    print_error('Failed to find provided engine, are you messing around?')
                    db.close()
                    return
                except Exception as e:
                    logger.error(f'Exporting engine {x} to {export_dir} failed: {str(e)}')
                    print_error(f'Failed to export engine {x}, corrupt?')
                    db.close()
                    return
            print_white(f'Engines exported to {export_dir}')
            return

        elif option == 'restore_backup':

            # Check if game is running, abort if it is
            if is_automation_running():
                clean_screen()
                logger.info('Aborted backup restore, game is running')
                print_warning('Game is running, please close it first.')
                return

            clean_screen()
            if confirm("Are you sure you want to restore a backup? This will delete the current save file.").ask():
                clean_screen()
                logger.info('Restoring backup')
                try:
                    SaveBackup().restore()
                    logger.info('Backup restored')
                    print_success('Backup restored.')
                    return
                except Exception as e:
                    logger.error(f'Failed to restore backup {str(e)}')
                    print_error('Failed to restore backup.')
                    return
            else:
                clean_screen()
                print_white('User aborted.')
                return

        elif option == 'options':
            while True:
                clean_screen()
                options = {
                    'type': 'list',
                    'name': 'option',
                    'message': 'Options',
                    'qmark': '@',
                    'choices': [
                        {
                            'name': 'Save path',
                            'value': 'save'
                        },
                        {
                            'name': 'Auto restore backup',
                            'value': 'backup_restore'
                        },
                        {
                            'name': 'MainMenu',
                            'value': 'return'
                        }
                    ]
                }
                answer = prompt(options, style=style)
                if not answer:
                    break
                clean_screen()
                option = answer['option']

                if not answer or answer['option'] == 'return':
                    clean_screen()
                    break

                elif option == 'save':

                    save_path = os.path.dirname(config.get('save_path'))
                    new_path = open_save(save_path)
                    config.set('save_path', new_path)
                    config.save()
                    clean_screen()

                elif option == 'backup_restore':

                    options = {
                        'type': 'list',
                        'name': 'option',
                        'message': 'Options',
                        'qmark': '@',
                        'choices': [
                            {
                                'name': 'Enable',
                                'value': True
                            },
                            {
                                'name': 'Disable',
                                'value': False
                            }
                        ]
                    }
                    if config.get('restore_backup_on_fail'):
                        print_white('Auto restore is currently: Enabled')
                    else:
                        print_white('Auto restore is currently: Disabled')
                    answer = prompt(options, style=style)

                    state = answer['option']

                    config.set('restore_backup_on_fail', state)
                    config.save()
                    clean_screen()

        elif option == 'quit':
            logger.info('Shutting down')
            sys.exit(0)


if __name__ == '__main__':
    clean_screen()
    logger.info('Booting up')
    print_white('Booting up...')
    config = data.ConfigData()
    ctypes.windll.kernel32.SetConsoleTitleW('Autocat Companion')
    clean_screen()
    print_maincolor('Welcome to Autocat Companion. {}'.format(version))
    print_warning('!!Warning, Do NOT run this program while the game is running.!!')
    print_warning('!!It\'s recommended to manually backup your savefile until this app is battletested.!!')
    while True:
        try:
            main()
        except KeyboardInterrupt:
            continue
