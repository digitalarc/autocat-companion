import os
import json

from pathlib import Path


def create_data_dir():
    homedir = str(Path.home())
    data_dir = os.path.join(homedir, 'AppData\\Local\\DigitalArc Studio\\Autocat Companion')

    if not os.path.isdir(data_dir):
        os.makedirs(os.path.dirname(data_dir), exist_ok=True)

    return data_dir


class ConfigData:
    """
    Abstraction of the configuration storage implemented in a singleton-like way to have easy global access and avoid re-reads
    :var instance: Global instance of the internal class for the data source
    :vartype instance: __DataSource
    """
    instance = None

    class __DataSource:
        """
        Configuration data source
        """
        def __init__(self):

            homedir = str(Path.home())
            default_save_dir = os.path.join(homedir, 'Documents\\My Games\\Automation\\Sandbox_openbeta.db')

            self.data_file = os.path.join(homedir, 'AppData\\Local\\DigitalArc Studio\\Autocat Companion\\config.json')

            data = {
                'config': {},
                'restore_backup_on_fail': True,
                'save_path': default_save_dir
            }
            self.data = data
            if not os.path.isfile(self.data_file):
                self.__create()
            else:
                with open(self.data_file, "r") as file:
                    self.data = json.load(file)

            settings = ['restore_backup_on_fail', 'save_path']
            for s in settings:
                self.data[s] = self.data.get(s, data.get(s))

        def save(self):
            """

            :return: None
            """
            if not os.path.isfile(self.data_file):
                self.__create()
            with open(self.data_file, "w") as file:
                json.dump(self.data, file, indent=4)

        def __create(self):
            """

            :return: None
            """
            os.makedirs(os.path.dirname(self.data_file), exist_ok=True)
            with open(self.data_file, "w") as write_file:
                json.dump(self.data, write_file, indent=4)

    def __init__(self):
        if not ConfigData.instance:
            ConfigData.instance = ConfigData.__DataSource()

    def get(self, key, default=None):
        """
        Retrieve data from the configuration
        :param key: Key to find in the config data
        :param default: Value to return if key is not present
        :return: Appropiate value or None
        """
        return self.instance.data.get(key, default)

    def set(self, key, value):
        """
        Set data to configuration
        :param key: Key to store the data on the config
        :param value: Value to store
        :return:  None
        """
        self.instance.data[key] = value

    def save(self):
        """
        Store the changes made on memory to the data file
        :return:  None
        """
        if self.instance:
            self.instance.save()

    def __iter__(self):
        return iter(self.instance.data.items()) if self.instance else iter({}.items())
