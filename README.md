# Autocat Companion
Import and export engines for [Automation - The Car Company Tycoon Game](https://store.steampowered.com/app/293760)

- [Releases/Download](https://gitlab.com/digitalarc/autocat-companion/-/releases)
- [Requirements](#requirements)
  - [System](#system)
  - [Packages](requirements.txt)
- [Setup](#setup)
- [Start](#start)
- [Config](#config)
- [Log](#log)
- [Backups](#backups)
- [Contributions](#contributions)
- [Changelog](CHANGELOG.md)
- [License](LICENSE)

## Requirements

#### System
- Python 3.9 64 bit, Other versions may work. Built on 3.9
- Windows 8/10

## Setup and installation below this is for development purpuses

## Setup
1. Clone this repo somewhere you want to "save it"

1. Install python and pip See [System](#system) for version.
                                                                                                                                                                                                                                                            
2. Install virtualenv
   ```text
   pip install virtualenv
   ```

3. Go to where you cloned this repo and start CMD from there
   ```text
   virtualenv venv
   venv\Scripts\activate
   ```
4. Install requirements
   ```text
   pip install -r requirements.txt
   ```
## Start 

1. Go to where you cloned this repo and start CMD from there
   ```text
   venv\Scripts\activate
   ```
2. Start
   ```text
   python app.py
   ```

## Config
Config is stored here
```text
%LOCALAPPDATA%\DigitalArc Studio\Autocat Companion\config.json
```

## Log
Log is stored here
```text
%LOCALAPPDATA%\DigitalArc Studio\Autocat Companion\history.log
```

## Backups
All save backups are stored here and rotated every 4 backup.
These can be moved to `%HOMEPATH%\Documents\My Games\Automation` by simply copying them and renaming to `Sandbox_openbeta.db`
```text
%LOCALAPPDATA%\DigitalArc Studio\Autocat Companion\Backups
```

## Contributions
All contributions are helpful, feel free to make a Merge Request.
